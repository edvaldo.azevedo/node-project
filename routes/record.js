var express = require('express');
var router = express.Router();
var Record = require('../models/records');

router.post('/', (req, res, next)=>{
    if(!req.body.username || !req.body.record){
        return res.status(422).send({error:'Faltam dados'});
    }

    var novoRecord = new Record({
        username:req.body.username,
        record: parseFloat(req.body.record)
    });

    novoRecord.save(err=>{
        if(err) return res.status(403).send({error:err});
        return res.send({record:novoRecord});
    })
});

router.get('/id/:identificacao', (req, res)=>{
    if(!req.params.identificacao){
        return res.status(422).send({error:'Faltam dados'});
    }
    Record.findById(req.params.identificacao).select('_id username record').exec((err,record)=>{
        if(err) return res.status(403).send({error:err});
        if(!record) return res.status(403).send({error:'Record não encontrado para este id'});
        return res.send({record});
    });
});

router.get('/token/:token', (req, res)=>{
    if(!req.params.token){
        return res.status(422).send({error:'Faltam dados'});
    }
    Record.findOne({
        token:req.params.token
    }).select('_id username record').exec((err,record)=>{
        if(err) return res.status(403).send({error:err});
        if(!record) return res.status(403).send({error:'Record não encontrado para este token'});
        return res.send({record});
    });
});

router.put('/', (req, res)=>{
    if(!req.body.token || !req.body.record){
        return res.status(422).send({error:'Faltam dados'});
    }
    Record.findOne({
        token:req.body.token
    }).exec((err, record)=>{
        if(err) return res.status(403).send({error:err});
        if(!record) return res.status(403).send({error:'Record não encontrado para este token'});
        if(parseFloat(req.body.record) > record.record){
            record.record = parseFloat(req.body.record);
        }
        record.save((err)=>{
            if(err) return res.status(403).send({error:err});
            return res.send({record})
        })
    })
})

module.exports = router;
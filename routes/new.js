var express = require('express');
const { route } = require('.');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('new', { title: 'Acessou' });
});

router.post('/cadastrar', function (req, res, next) {
    console.log(req.body.nome);
    console.log(req.body.idade);

    const nome = req.body.nome;
    const idade = parseInt(req.body.idade);
    const uf = req.body.uf;
    global.db.insert({ nome, idade, uf }, (err, result) => {
        if (err) { return console.log(err); }
        res.redirect('/');
    })

});

router.get('/delete/:id', function(req, res){
    var id = req.params.id;
    console.log(id);
    global.db.deleteOne(id, (e, r) => {
    if(e) { return console.log(e); }
    res.redirect('/');
    });
});

module.exports = router;
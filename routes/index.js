var express = require('express');
var router = express.Router();
var Record = require('../models/records');

/* GET home page. */
router.get('/', function(req, res, next) {

  global.db.findAll((e,docs)=>{
    if(e){return console.log(e);}
    res.render('index',{docs:docs});
  })

});

module.exports = router;

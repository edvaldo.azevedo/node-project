var express=require('express');
var router=express.Router();

var fs = require('fs');

router.get('/', function (req, res, next) {
    readText(req, res);
    console.log('Requisicao terminou');
});


function readText(request, response) {
    //salve um arquivo teste.txt junto a esse arquivo com qualquer coisa dentro
    fs.readFile('./public/teste.txt', function (err, data) {
        if (err) {
            console.log('erro na leitura')
            return response.status(500).send('Erro ao ler o arquivo.')
        }
        response.write(data)
        response.end();
        console.log('leu arquivo')
    });
    console.log('Lendo o arquivo, aguarde.')
}

module.exports=router;
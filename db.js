const { ObjectID } = require('mongodb');

var mongoClient = require('mongodb').MongoClient;
mongoClient.connect('mongodb://root123:mudar123@geonosis.mongodb.umbler.com:46840/app-test')
 .then(conn => global.conn = conn)
 .catch(err => console.log(err))

 function findAll(callback){

    global.conn.collection('customers').find().toArray(callback);
 }

 function insert(customer, callback){
    global.conn.collection('customers').insert(customer, callback);
}

function deleteOne(id, callback){
    global.conn.collection('customers').deleteOne({_id:  new ObjectID (id)},
   callback);
   }
module.exports = { findAll, insert,  deleteOne }
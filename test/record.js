var chai = require('chai');
var server = require('../app');
var chaiHttp = require('chai-http');
var Record = require('../models/records');
var should = chai.should();
chai.use(chaiHttp);

describe('Teste endpoint /record',()=>{

    var id;
    var token;

    before((next)=>{
        Record.remove({}, (err)=>{
            next();
        });
    });

    it('Test endpoint POST', (done)=>{

        var novoRecord = {
            username:'edvaldo',
            record:200
        }
        chai.request(server).post('/record').send(novoRecord).end((req, res)=>{
            res.should.have.status(200);
            res.body.should.have.property('record');

            res.body.record.should.have.property('_id');
            res.body.record.should.have.property('token');
            res.body.record.should.have.property('username');
            res.body.record.should.have.property('record');

            res.body.record.username.should.be.equal('edvaldo');
            res.body.record.record.should.be.equal(200);
            
             token = res.body.record.token;
             id = res.body.record._id;
            done();
        });
    });

    it('Teste record by id', (done)=>{

        chai.request(server).get('/record/id/'+id).end((req, res)=>{
            
            console.log(res.body);

            res.should.have.status(200);
            res.body.should.have.property('record');

            res.body.record.should.have.property('_id');
            res.body.record.should.have.property('username');
            res.body.record.should.have.property('record');

            res.body.record.username.should.be.equal('edvaldo');
            res.body.record.record.should.be.equal(200);
            
           
        done();
        });
    });

    it('Teste buscar record by token', (done)=>{
        chai.request(server).get('/record/token/'+token).end((req, res)=>{
            res.should.have.status(200);
            res.body.should.have.property('record');

            res.body.record.should.have.property('_id');
            res.body.record.should.have.property('username');
            res.body.record.should.have.property('record');

            res.body.record.username.should.be.equal('edvaldo');
            res.body.record.record.should.be.equal(200);
            
           
        done();
        });
    });
    
    it('Teste alterar record maior', (done)=>{

        var newRecord = {
            token:token,
            record:400
        }
        chai.request(server).put('/record/').send(newRecord).end((req, res)=>{
            
            console.log(res.body);

            res.should.have.status(200);
            res.body.should.have.property('record');

            res.body.record.should.have.property('_id');
            res.body.record.should.have.property('username');
            res.body.record.should.have.property('record');

            res.body.record.username.should.be.equal('edvaldo');
            res.body.record.record.should.be.equal(400);
            
           
        done();
        });
    });

    it('Teste alterar record menor', (done)=>{
        var newRecord = {
            token:token,
            record:300
        }
        chai.request(server).put('/record/').send(newRecord).end((req, res)=>{
            
            console.log(res.body);

            res.should.have.status(200);
            res.body.should.have.property('record');

            res.body.record.should.have.property('_id');
            res.body.record.should.have.property('username');
            res.body.record.should.have.property('record');

            res.body.record.username.should.be.equal('edvaldo');
            res.body.record.record.should.be.equal(400);
            
           
        done();
        });
    });
});
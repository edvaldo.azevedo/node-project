const chai = require('chai');

const expect =  chai.expect;

const somarDoisNumeros = (a,b)=>{
    if(typeof a ==="number" && typeof b ==="number") 
    return a + b;
    else return undefined;
} 
    

describe('Somas', ()=>{

    it('Somas de dois numeros 2 e 3', (done)=>{
        const resultado  = somarDoisNumeros(2,3);
        expect(resultado).be.equals(5);
        done();
    });

    it('Somas de dois numeros -2 e 3', (done)=>{
        const resultado  = somarDoisNumeros(-2,3);
        expect(resultado).be.equals(1);
        done();
    });

    it('Somas de dois numeros -2 e 3', (done)=>{
        const resultado  = somarDoisNumeros(-2,3);
        expect(resultado).be.equals(1);
        done();
    });
    it('Somas de dois numeros tipos inválidos "teste" e 3 ', (done)=>{
        const resultado  = somarDoisNumeros("teste",3);
        expect(resultado).be.equal(undefined);
        done();
    });

    it('Somas de dois numeros tipos inválidos null e 3 ', (done)=>{
        const resultado  = somarDoisNumeros(null,3);
        expect(resultado).be.equal(undefined);
        done();
    });
    
});
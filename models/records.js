var crypto = require('crypto');
var mongoose = require('mongoose');

var schemaRecord = new mongoose.Schema({

    username:{
        type: String,
        required:true,
        unique:true
    },
    record:{
        type:Number,
        required:true
    },
    token:{
        type: String
    }
},
{
    timestamp:true
});

schemaRecord.pre('save', function(next){

    if(!this.token){
        this.token = crypto.randomBytes(64).toString('hex');
        next(null);
    }
    next(null);
});

module.exports = mongoose.model('Record', schemaRecord);